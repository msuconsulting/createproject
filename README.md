**Author:** Michael Lerch   
**Description:** The package `CreateProject` sets up a folder structure and creates a main README.md skeleton. We can talk to Michael about adjustments if we want to change the structure. 

**Instructions for installation**   

1. Install the package `devtools`. You only have to do this _one time_ and it can be 
accomplished by running `install.packages('devtools')` in the R console. Let that finish and then run `library(devtools)` this will load the package. _Anytime_ you want to use something from the `devtools` package you must run `library(devtools)` first. 

2. Now install `CreatePackage`. Run the following lines to install and load the package   
`install_bitbucket("MSUconsulting/CreateProject")`   
`library(CreateProject)`   
You only have to install the package once, but anytime you want to use the `createproject()` function you must first run `library(CreateProject)`

3. Now you can use the package!    

__Instructions for using the `createproject()` function__

`# if folder already exists for new project:`   
`setwd("folder_name_for_new_project")`    
`createproject()`  

`# if folder does not exist for new project yet:`      
`setwd("folder_name_for_all_consulting_projects")`    
`createproject("New_project_folder_name")`